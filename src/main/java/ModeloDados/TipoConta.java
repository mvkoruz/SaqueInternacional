package ModeloDados;

public enum TipoConta {
	Basic("Basic"),
	Premium("Premium");
	
	private final String tipo;
	
	TipoConta(String tipo) {
		this.tipo = tipo;
	}
	
	public String toString() {
		return this.tipo;
	}
}
