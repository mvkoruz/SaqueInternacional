package io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ioArq {

	public List<String> lista;

	public List<String> readArq(String arq) {
		Path path = Paths.get(arq);
		
		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}
		//retirar o cabeçalho
		lista.remove(0);

		return lista;
	}
	
	public List<String> writeArq(String arq, List<String> lista) {
		Path path = Paths.get(arq);
		
		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}
		//retirar o cabeçalho
		lista.remove(0);

		return lista;
	}
}
