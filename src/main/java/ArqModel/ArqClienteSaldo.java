package ArqModel;

import java.util.ArrayList;
import java.util.List;

import ModeloDados.Cliente;
import ModeloDados.TipoConta;
import io.ioArq;

public class ArqClienteSaldo {
	private ioArq arqClienteSaldo = new ioArq();
	private List<String> lista;

	public ArqClienteSaldo() {
		// name, username, password, balance, type
		// 0   , 1       , 2       , 3      , 4

		lista = arqClienteSaldo.readArq("accounts.csv");

		if(lista == null) {
			// enviar exception
			System.out.println("Não foi possível ler o arquivo");
			return;
		}
	}

	public List<Cliente> readArqSaldo(){
		List<Cliente> clientes = new ArrayList<Cliente>();
		for(String linha: lista) {
			String[] partes = linha.split(",");
			
			Cliente cliente = new Cliente();
			cliente.setNome(partes[0]);
			cliente.setUsername(partes[1]);
			cliente.setPassword(partes[2]);
			cliente.setSaldo(Double.parseDouble(partes[3]));
			
			if(partes[4] == "Basic") {
				cliente.setTipoConta(TipoConta.Basic);
			}
			else{
				cliente.setTipoConta(TipoConta.Premium);
			}
			clientes.add(cliente);
		}
		return clientes;
	}
}
