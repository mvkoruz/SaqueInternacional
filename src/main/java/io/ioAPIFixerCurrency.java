package io;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import APIModel.APIFixerRates;


public class ioAPIFixerCurrency {
	private String urlBase = "http://data.fixer.io/api/latest?access_key=";
	private String accessKey = "578ac533a860ac0f3d508e3dbad0ef57";
	private String url = urlBase + accessKey;
	private Gson gson = new Gson();
	private String response;
	APIFixerRates json = new APIFixerRates();
	
	public ioAPIFixerCurrency(){
		gson = new Gson();
		response = HttpRequest.get(url).body();			
		json = gson.fromJson(response, APIFixerRates.class);
	}
	
	public APIFixerRates getCotacoes() {
		return json;
	}
	
	public Double getCotacao(String moeda)
	{
		return json.rates.get(moeda);
	}
	
	public Double Converter(String deMoeda, String paraMoeda) {
		return json.rates.get(deMoeda) / json.rates.get(paraMoeda);
	}
}
